FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
	file://weston-libinput.ini \
"

do_install_append() {
	#bbplain "will cat weston-libinput.ini into ${D}/etc/xdg/weston/weston.ini";
	cat ${WORKDIR}/weston-libinput.ini >> ${D}/etc/xdg/weston/weston.ini
}
