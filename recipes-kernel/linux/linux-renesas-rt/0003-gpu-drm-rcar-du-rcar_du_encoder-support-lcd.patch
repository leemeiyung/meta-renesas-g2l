From 65fc2a76e1dc7fa2867adcbbe386442909599f35 Mon Sep 17 00:00:00 2001
From: jiangj <jiangj@vanxoak.com>
Date: Tue, 23 Nov 2021 10:17:00 +0800
Subject: [PATCH] gpu/drm/rcar-du/rcar_du_encoder support lcd

---
 drivers/gpu/drm/rcar-du/rcar_du_encoder.c | 185 ++++++++++++++++++++--
 1 file changed, 168 insertions(+), 17 deletions(-)

diff --git a/drivers/gpu/drm/rcar-du/rcar_du_encoder.c b/drivers/gpu/drm/rcar-du/rcar_du_encoder.c
index a5f5d19fff4f..e1dc577a653a 100644
--- a/drivers/gpu/drm/rcar-du/rcar_du_encoder.c
+++ b/drivers/gpu/drm/rcar-du/rcar_du_encoder.c
@@ -18,6 +18,7 @@
 #include <drm/drm_crtc.h>
 #include <drm/drm_crtc_helper.h>
 #include <drm/drm_panel.h>
+#include <drm/drm_atomic_helper.h>
 
 #include "rcar_du_drv.h"
 #include "rcar_du_encoder.h"
@@ -48,13 +49,81 @@ static const struct drm_encoder_funcs encoder_funcs = {
 	.destroy = drm_encoder_cleanup,
 };
 
-int rcar_du_encoder_init(struct rcar_du_device *rcdu,
+/* add by jiangj support for parallel lcd */
+struct rzg2l_lcd {
+	struct drm_connector	connector;
+	struct drm_panel *panel;
+	struct rcar_du_encoder renc;
+};
+
+static inline struct rzg2l_lcd *drm_connector_to_rzg2l_lcd(struct drm_connector *connector)
+{
+	return container_of(connector, struct rzg2l_lcd, connector);
+}
+
+static inline struct rzg2l_lcd *drm_rcar_encoder_to_rzg2l_lcd(struct rcar_du_encoder *renc)
+{
+	return container_of(renc, struct rzg2l_lcd, renc);
+}
+
+static void rcar_du_encoder_enable(struct drm_encoder *encoder)
+{
+	struct rcar_du_encoder *renc = to_rcar_encoder(encoder);
+	struct rzg2l_lcd *g2l_lcd = drm_rcar_encoder_to_rzg2l_lcd(renc);
+
+	DRM_DEBUG_DRIVER("Enabling RGB output\n");
+
+	if (!IS_ERR(g2l_lcd->panel)) {
+		drm_panel_prepare(g2l_lcd->panel);
+		drm_panel_enable(g2l_lcd->panel);
+	}
+}
+
+static void rcar_du_encoder_disable(struct drm_encoder *encoder)
+{
+	struct rcar_du_encoder *renc = to_rcar_encoder(encoder);
+	struct rzg2l_lcd *g2l_lcd = drm_rcar_encoder_to_rzg2l_lcd(renc);
+
+	DRM_DEBUG_DRIVER("Disabling RGB output\n");
+
+	if (!IS_ERR(g2l_lcd->panel)) {
+		drm_panel_disable(g2l_lcd->panel);
+		drm_panel_unprepare(g2l_lcd->panel);
+	}
+}
+
+static const struct drm_encoder_helper_funcs encoder_lcd_helper_funcs = {
+	.disable	= rcar_du_encoder_disable,
+	.enable		= rcar_du_encoder_enable,
+	.mode_valid = rcar_du_encoder_mode_valid,
+};
+
+static int rzg2l_lcd_connector_get_modes(struct drm_connector *connector)
+{
+	struct rzg2l_lcd *g2l_lcd = drm_connector_to_rzg2l_lcd(connector);
+
+	return drm_panel_get_modes(g2l_lcd->panel);
+}
+
+
+static const struct drm_connector_helper_funcs rzg2l_lcd_conn_helper_funcs = {
+        .get_modes = rzg2l_lcd_connector_get_modes,
+};
+
+static const struct drm_connector_funcs rzg2l_lcd_conn_funcs = {
+	.reset = drm_atomic_helper_connector_reset,
+	.fill_modes = drm_helper_probe_single_connector_modes,
+	.destroy = drm_connector_cleanup,
+	.atomic_duplicate_state = drm_atomic_helper_connector_duplicate_state,
+	.atomic_destroy_state = drm_atomic_helper_connector_destroy_state,
+};
+
+static int rcar_du_bridge_encoder_init(struct rcar_du_device *rcdu,
 			 enum rcar_du_output output,
-			 struct device_node *enc_node)
+			 struct device_node *enc_node, struct drm_bridge *bridge)
 {
 	struct rcar_du_encoder *renc;
 	struct drm_encoder *encoder;
-	struct drm_bridge *bridge = NULL;
 	int ret;
 
 	renc = devm_kzalloc(rcdu->dev, sizeof(*renc), GFP_KERNEL);
@@ -65,16 +134,6 @@ int rcar_du_encoder_init(struct rcar_du_device *rcdu,
 	renc->output = output;
 	encoder = rcar_encoder_to_drm_encoder(renc);
 
-	dev_dbg(rcdu->dev, "initializing encoder %pOF for output %u\n",
-		enc_node, output);
-
-	/* Locate the DRM bridge from the encoder DT node. */
-	bridge = of_drm_find_bridge(enc_node);
-	if (!bridge) {
-		ret = -EPROBE_DEFER;
-		goto done;
-	}
-
 	/*
 	 * On Gen3 skip the LVDS1 output if the LVDS1 encoder is used as a
 	 * companion for LVDS0 in dual-link mode.
@@ -86,6 +145,10 @@ int rcar_du_encoder_init(struct rcar_du_device *rcdu,
 		}
 	}
 
+	/*
+	 * Attach the bridge to the encoder. The bridge will create the
+	 * connector.
+	 */
 	ret = drm_encoder_init(rcdu->ddev, encoder, &encoder_funcs,
 			       DRM_MODE_ENCODER_NONE, NULL);
 	if (ret < 0)
@@ -93,10 +156,6 @@ int rcar_du_encoder_init(struct rcar_du_device *rcdu,
 
 	drm_encoder_helper_add(encoder, &encoder_helper_funcs);
 
-	/*
-	 * Attach the bridge to the encoder. The bridge will create the
-	 * connector.
-	 */
 	ret = drm_bridge_attach(encoder, bridge, NULL);
 	if (ret) {
 		drm_encoder_cleanup(encoder);
@@ -111,4 +170,96 @@ int rcar_du_encoder_init(struct rcar_du_device *rcdu,
 	}
 
 	return ret;
+	
+}
+
+/* add by jiangj support for parallel lcd */			 
+static int rcar_du_lcd_encoder_init(struct rcar_du_device *rcdu,
+			 enum rcar_du_output output,
+			 struct device_node *enc_node, struct drm_panel *panel)
+{
+	struct rzg2l_lcd *g2l_lcd;
+	struct drm_connector *connector;
+	
+	struct rcar_du_encoder *renc;
+	struct drm_encoder *encoder;
+	int ret;
+
+	g2l_lcd = devm_kzalloc(rcdu->dev, sizeof(*g2l_lcd), GFP_KERNEL);
+	if (!g2l_lcd) {
+		return -ENOMEM;
+	}
+
+	renc = &g2l_lcd->renc;
+	renc->output = output;
+	encoder = rcar_encoder_to_drm_encoder(renc);
+
+	ret = drm_encoder_init(rcdu->ddev, encoder, &encoder_funcs,
+			       DRM_MODE_ENCODER_NONE, NULL);
+	if (ret < 0)
+		goto done;
+
+	drm_encoder_helper_add(encoder, &encoder_lcd_helper_funcs);
+	
+	connector = &g2l_lcd->connector;
+	ret = drm_connector_init(rcdu->ddev, connector,
+				 &rzg2l_lcd_conn_funcs,
+				 DRM_MODE_CONNECTOR_Unknown);
+	if (ret) {
+		drm_encoder_cleanup(encoder);
+		devm_kfree(rcdu->dev, g2l_lcd);
+		return ret;
+	}
+
+	drm_connector_helper_add(connector, &rzg2l_lcd_conn_helper_funcs);
+
+	ret = drm_connector_attach_encoder(connector, encoder);
+	if (ret) {
+		drm_connector_cleanup(connector);
+		drm_encoder_cleanup(encoder);
+		devm_kfree(rcdu->dev, g2l_lcd);
+		return ret;
+	}
+
+	ret = drm_panel_attach(panel, connector);	
+	if (ret) {
+		drm_connector_cleanup(connector);
+		drm_encoder_cleanup(encoder);
+		devm_kfree(rcdu->dev, g2l_lcd);
+		return ret;
+	}
+	
+	g2l_lcd->panel = panel;	
+	rcdu->encoders[output] = renc;
+
+done:
+	if (ret < 0) {
+		if (encoder->name)
+			encoder->funcs->destroy(encoder);
+		devm_kfree(rcdu->dev, g2l_lcd);
+	}
+
+	return ret;
+}
+
+int rcar_du_encoder_init(struct rcar_du_device *rcdu,
+			 enum rcar_du_output output,
+			 struct device_node *enc_node)
+{
+	struct drm_panel *panel = NULL;
+	struct drm_bridge *bridge = NULL;
+	
+	dev_dbg(rcdu->dev, "initializing encoder %pOF for output %u\n",
+		enc_node, output);
+
+	/* Locate the DRM bridge from the encoder DT node. */
+	bridge = of_drm_find_bridge(enc_node);
+	if (bridge)
+		return rcar_du_bridge_encoder_init(rcdu, output, enc_node, bridge);
+		
+	panel = of_drm_find_panel(enc_node);
+	if (!IS_ERR(panel))
+		return rcar_du_lcd_encoder_init(rcdu, output, enc_node, panel);
+		
+	return -EPROBE_DEFER;
 }
