DESCRIPTION = "Linux kernel for the RZG2 based board"

require recipes-kernel/linux/linux-yocto.inc

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}/:"
CUSTOMIZEFILESDIR := "${THISDIR}/${PN}"
COMPATIBLE_MACHINE = "(smarc-rzg2l|rzg2l-dev|rzg2lc-dev|smarc-rzg2lc|g2l_iot|g2l_iot_rt|g2l_iot_1gddr)"

KERNEL_URL = " \
    git://github.com/renesas-rz/rz_linux-cip.git"
BRANCH = "rz-cip41-rt"
SRCREV = "cbca9cb548ae49cafaed45dfe682269920926776"

SRC_URI = "${KERNEL_URL};protocol=https;nocheckout=1;branch=${BRANCH}"
SRC_URI_append = " \
	file://g2l_iot_rt_defconfig \
	file://rzg2l-smarc-dev-iot.dtsi \
	file://g2l-iot-lcd.dts \
	file://g2l-iot-lt8912.dts \
	file://0001-add-panel-raydium-rm67191-support.patch \
	file://0003-gpu-drm-rcar-du-rcar_du_encoder-support-lcd.patch \
	file://0004-add-support-lt8912-bridge.patch \
	file://0008-net-ethernet-renesas-ravb_main.c-add-ravb_reset_phy.patch \
	file://0009-add-motorcomm-phy-support.patch \
"

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"
LINUX_VERSION ?= "4.19.165"

PV = "${LINUX_VERSION}+git${SRCPV}"
PR = "r1"

KBUILD_DEFCONFIG = "defconfig"
KCONFIG_MODE = "alldefconfig"

do_kernel_metadata_prepend() {
	bbplain "linux-renesas_rt_4.19.bb will copy defconfig files to ${S}/arch/arm64/configs"
	cp ${CUSTOMIZEFILESDIR}/g2l_iot_rt_defconfig ${S}/arch/arm64/configs
	bbplain "linux-renesas_rt_4.19.bb will copy dts files to ${S}/arch/arm64/boot/dts/renesas"
	cp ${CUSTOMIZEFILESDIR}/rzg2l-smarc-dev-iot.dtsi ${S}/arch/arm64/boot/dts/renesas
	cp ${CUSTOMIZEFILESDIR}/g2l-iot*.dts ${S}/arch/arm64/boot/dts/renesas
	bbplain "linux-renesas_rt_4.19.bb will check ${S}/arch/arm64/boot/dts/renesas/Makefile"
	if [ `grep -c " g2l-iot-lcd.dtb" ${S}/arch/arm64/boot/dts/renesas/Makefile` -eq '1' ]; then
		bbnote "already added compile for g2l-iot-lcd.dtb"
	else
		bbnote "will add compile for g2l-iot-lcd.dtb"
		sed -i '/dtb-$(CONFIG_ARCH_R9A07G044L2)/i\dtb-$(CONFIG_ARCH_R9A07G044L2) += g2l-iot-lcd.dtb g2l-iot-lt8912.dtb' ${S}/arch/arm64/boot/dts/renesas/Makefile
	fi
}

do_kernel_metadata_af_patch() {
	# need to recall do_kernel_metadata after do_patch for some patches applied to defconfig
	rm -f ${WORKDIR}/defconfig
	do_kernel_metadata
}

addtask do_kernel_metadata_af_patch after do_patch before do_kernel_configme

do_srcexport() {
	cd ${STAGING_KERNEL_DIR} && cp ${STAGING_KERNEL_BUILDDIR}/.config .config && tar cvzf ${DEPLOY_DIR}/linux-renesas-rt-4.19.tar.gz --exclude=oe-* * .config && rm .config
}
addtask srcexport after patch

# Fix race condition, which can causes configs in defconfig file be ignored
do_kernel_configme[depends] += "virtual/${TARGET_PREFIX}binutils:do_populate_sysroot"
do_kernel_configme[depends] += "virtual/${TARGET_PREFIX}gcc:do_populate_sysroot"
do_kernel_configme[depends] += "bc-native:do_populate_sysroot bison-native:do_populate_sysroot"

# Fix error: openssl/bio.h: No such file or directory
DEPENDS += "openssl-native"
PROVIDES += "linux-renesas"

