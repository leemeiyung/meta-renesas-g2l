// SPDX-License-Identifier: GPL-2.0
/*
 * Device Tree Source for the RZG2L Dual A55 Evaluation board
 *
 * Copyright (C) 2021 Renesas Electronics Corp.
 */

/dts-v1/;
#include "r9a07g044l2.dtsi"

#define SDHI1_SUPPORT		1
#define GPT_SUPPORT		1

/* Enable/Disable CAN support (must choose 1) */
#define CAN_SUPPORT		1
//#define CAN_SUPPORT		0

#define DISP_PARALLEL_HDMI	0   
#define DISP_PARALLEL_PANEL	1	//rgb

/* Besides that, LCDC can only output 1 video interface (Parallel or MIPI DSI).
 * Because parallel display is disabled as above, we enable MIPI DSI as below.
 * Please choose one of two interfaces (HDMI or panel) based on your board
 * by setting 1 in value.
 * Currently we support HDMI as default output.
 */
#define DISP_DSI_HDMI		0 //mipi2hdmi
#define DISP_DSI_PANEL		0

#include "rzg2l-smarc-dev-iot.dtsi"

/ {
	model = "Evaluation board based on r9a07g044l2";
	compatible = "renesas,r9a07g044l2-dev", "renesas,r9a07g044l2";

	memory@48000000 {
		device_type = "memory";
		/* first 128MB is reserved for secure area. */
		reg = <0x0 0x48000000 0x0 0x78000000>;
	};

	reserved-memory {
		#address-cells = <2>;
		#size-cells = <2>;
		ranges;

		reserved: linux,cma@58000000 {
			compatible = "shared-dma-pool";
			linux,cma-default;
			reusable;
			reg = <0x0 0x58000000 0x0 0x10000000>;
		};
		mmp_reserved: linux,multimedia {
			compatible = "shared-dma-pool";
			reusable;
			reg = <0x00000000 0x68000000 0x0 0x8000000>;
		};
	};

	mmngr {
		compatible = "renesas,mmngr";
		memory-region = <&mmp_reserved>;
	};

	mmngrbuf {
		compatible = "renesas,mmngrbuf";
	};

	vspm_if {
			compatible = "renesas,vspm_if";
	};
};

