DESCRIPTION = "Linux kernel for the RZG2 based board"

COMPATIBLE_MACHINE = "(smarc-rzg2l|rzg2l-dev|rzg2lc-dev|smarc-rzg2lc|g2l_iot|g2l_iot_1gddr)"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
CUSTOMIZEFILESDIR := "${THISDIR}/${PN}"

SRC_URI_append = " \
	file://g2l_iot_defconfig \
	file://g2l_iot_sd_defconfig \
	file://rzg2l-smarc-dev-iot.dtsi \
	file://g2l-iot.dts \
	file://g2l-iot-lcd.dts \
	file://g2l-iot-1gddr.dts \
	file://0001-add-panel-raydium-rm67191-support.patch \
	file://0002-add-cyttsp5-touchscreen-support.patch \
	file://0003-gpu-drm-rcar-du-rcar_du_encoder-support-lcd.patch \
	file://0004-add-support-lt8912-bridge.patch \
	file://0005-wm8960-enable-gpio1-to-prevent-ADCLRC.patch \
"

SRC_URI += "${@'file://0005-rzg2l-PIN_CONFIG_BIAS_PULL_UP-or-DOWN-which-merge-fr.patch ' if '${SRCREV}' == 'c819d510b2b02511f083023bcd8d6cd41f9d003c' else ' '}"

do_kernel_metadata_prepend() {
	bbplain "linux-renesas_4.19.bbappend will copy defconfig files to ${S}/arch/arm64/configs"
	cp ${CUSTOMIZEFILESDIR}/g2l_iot_defconfig ${S}/arch/arm64/configs
	cp ${CUSTOMIZEFILESDIR}/g2l_iot_sd_defconfig ${S}/arch/arm64/configs
	bbplain "linux-renesas_4.19.bbappend will copy dts files to ${S}/arch/arm64/boot/dts/renesas"
	cp ${CUSTOMIZEFILESDIR}/rzg2l-smarc-dev-iot.dtsi ${S}/arch/arm64/boot/dts/renesas
	cp ${CUSTOMIZEFILESDIR}/g2l-iot*.dts ${S}/arch/arm64/boot/dts/renesas
	bbplain "linux-renesas_4.19.bbappend will check ${S}/arch/arm64/boot/dts/renesas/Makefile"
	if [ `grep -c " g2l-iot.dtb" ${S}/arch/arm64/boot/dts/renesas/Makefile` -eq '1' ]; then
		bbnote "already added compile for g2l-iot.dtb"
	else
		bbnote "will add compile for g2l-iot.dtb"
		sed -i '/dtb-$(CONFIG_ARCH_R9A07G044L2)/i\dtb-$(CONFIG_ARCH_R9A07G044L2) += g2l-iot.dtb g2l-iot-lcd.dtb g2l-iot-1gddr.dtb' ${S}/arch/arm64/boot/dts/renesas/Makefile
	fi
}
addtask showvars
do_showvars[nostamp] = "1"
python do_showvars(){
    # emit only the metadata that are variables and not functions
    isfunc = lambda key: bool(d.getVarFlag(key, 'func'))
    vars = sorted((key for key in bb.data.keys(d) \
        if not key.startswith('__')))
    for var in vars:
        if not isfunc(var):
            try:
                val = d.getVar(var, True)
            except Exception as exc:
                bb.plain('Expansion of %s threw %s: %s' % \
                    (var, exc.__class__.__name__, str(exc)))
            bb.plain('%s="%s"' % (var, val))
}
