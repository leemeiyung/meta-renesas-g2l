
FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "\
    file://rc.local \
	file://mdioAdjust.sh \
	file://after-linkup.conf \
"

do_install_append () {
	install -d ${D}${sysconfdir}/systemd/system/multi-user.target.wants/
	ln -sf ${systemd_unitdir}/system/rc-local.service \
					${D}${sysconfdir}/systemd/system/multi-user.target.wants/rc-local.service
	install -D -m0755 ${WORKDIR}/rc.local ${D}${sysconfdir}/rc.local
	install -D -m0755 ${WORKDIR}/mdioAdjust.sh ${D}${sysconfdir}/mdioAdjust.sh
	install -d ${D}${sysconfdir}/systemd/system/systemd-networkd.service.d/
	install -D -m0644 ${WORKDIR}/after-linkup.conf ${D}${sysconfdir}/systemd/system/systemd-networkd.service.d/after-linkup.conf

}

FILES_${PN} += "\
	${sysconfdir}/systemd/system/multi-user.target.wants/rc-local.service \
    ${sysconfdir}/rc.local \
	${sysconfdir}/mdioAdjust.sh \
	${sysconfdir}/systemd/system/systemd-networkd.service.d/after-linkup.conf \
"