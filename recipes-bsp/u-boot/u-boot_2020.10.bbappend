LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
	${@' file://0001-smarc-rzg2l-support-1g-ddr.patch ' if '${MACHINE}' == 'g2l_iot_1gddr' else ' '} \
	file://0002-add-g2l_iot-support.patch \
	file://0003-add-beep-control.patch \
	"

