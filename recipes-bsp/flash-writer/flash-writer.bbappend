FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "${@' file://0001-add-support-g2l_iot-and-g2l_iot_1gddr.patch ' if '${SRCREV}' == 'cb40eb7529dfab5c049810ea0d59635080d7ecdb' else ' '} \
            ${@' file://0001-add-supoort-G2L_IOT-and-G2L_IOT_1GDDR-board-support.patch ' if '${SRCREV}' == '1b460ff17ead93ffa44b03f5fc737cbc2e473b14' else ' '}  \
            "
do_compile() {
        if [ "${MACHINE}" = "smarc-rzg2l" ]; then
                BOARD="RZG2L_SMARC";
                PMIC_BOARD="RZG2L_SMARC_PMIC";
		elif [ "${MACHINE}" = "g2l_iot" ]; then
				BOARD="G2L_IOT";
		elif [ "${MACHINE}" = "g2l_iot_rt" ]; then
				BOARD="G2L_IOT";				
		elif [ "${MACHINE}" = "g2l_iot_1gddr" ]; then
				BOARD="G2L_IOT_1GDDR";			
        elif [ "${MACHINE}" = "rzg2l-dev" ]; then
                BOARD="RZG2L_15MMSQ_DEV";
        elif [ "${MACHINE}" = "rzg2lc-dev" ]; then
                BOARD="RZG2LC_DEV";
        elif [ "${MACHINE}" = "smarc-rzg2lc" ]; then
                BOARD="RZG2LC_DEV";
        fi
        cd ${S}

	oe_runmake BOARD=${BOARD} EMMC=ENABLE

        if [ "${PMIC_SUPPORT}" = "1" ]; then
		oe_runmake OUTPUT_DIR=${PMIC_BUILD_DIR} clean;
		oe_runmake BOARD=${PMIC_BOARD} OUTPUT_DIR=${PMIC_BUILD_DIR};
	fi
}