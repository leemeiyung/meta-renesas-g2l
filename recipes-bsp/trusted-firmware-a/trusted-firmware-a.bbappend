
COMPATIBLE_MACHINE = "(smarc-rzg2l|rzg2l-dev|smarc-rzg2lc|rzg2lc-dev|g2l_iot|g2l_iot_rt|g2l_iot_1gddr)"

PLATFORM ?= "g2l"
PLATFORM_g2l_iot = "g2l"
EXTRA_FLAGS_g2l_iot = "BOARD=smarc_2"
PMIC_EXTRA_FLAGS_g2l_iot = "BOARD=smarc_pmic_2"
FLASH_ADDRESS_BL2_BP_g2l_iot = "00000"
FLASH_ADDRESS_FIP_g2l_iot = "1D200"

PLATFORM ?= "g2l"
PLATFORM_g2l_iot_rt = "g2l"
EXTRA_FLAGS_g2l_iot_rt = "BOARD=smarc_2"
PMIC_EXTRA_FLAGS_g2l_iot_rt = "BOARD=smarc_pmic_2"
FLASH_ADDRESS_BL2_BP_g2l_iot_rt = "00000"
FLASH_ADDRESS_FIP_g2l_iot_rt = "1D200"

PLATFORM_g2l_iot_1gddr = "g2l"
EXTRA_FLAGS_g2l_iot_1gddr = "BOARD=g2l_iot_1gddr"
PMIC_EXTRA_FLAGS_g2l_iot_1gddr = "BOARD=smarc_pmic_2"
FLASH_ADDRESS_BL2_BP_g2l_iot_1gddr = "00000"
FLASH_ADDRESS_FIP_g2l_iot_1gddr = "1D200"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://0001-sync-source-with-renesas-which-support-emmc-boot.patch \
            file://0002-add-ddr-param-for-g2l_iot_1gddr.patch \
            file://0003-modify-spi_multi_setup-from-dummy_10cycle-to-dummy_6.patch \
            "

SRC_URI += "${@'file://0001-Second-attempt-to-fix-flickering.patch ' if '${SRCREV}' == '8e2054e9cff87b889eeea2679d0fd47faa04c195' else ' '}"
